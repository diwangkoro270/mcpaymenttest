import java.util.Arrays;

public class MCPayment {
	public static void main(String[] args) {
		no1();
		no2();
		no3();
	}

	public static void no1(){
		int[] nums = new int[]{3,1,4,2,5,1,2,37,8,5};
		int[] ret = getMostPositiveNumber(nums);

		System.out.println("Jawaban nomor 1: "+ Arrays.toString(ret));
	}

	public static void no2(){
		int[] nums = new int[]{3,1,4,2,5,1,2,31,8,5};
		int x = 4;
		int[] ret = getNumsDivisionWithoutX(nums,x);
		
		System.out.println("Jawaban nomor 2: "+Arrays.toString(ret));		 
	}

	public static void no3() {
		String word = "Souvenir loud four lost";
		int x = 4;
		String[] ret = getStringWithXlength(word,x);

		System.out.println("Jawaban nomor 3: "+Arrays.toString(ret));	
	}

	public static int[] getMostPositiveNumber(int[] input){
		// Without collections
		int[] arrTemp = new int[input.length];

		int sum = 0;
		for (int each:input){
			for (int i=0;i<input.length;i++){
				if (each-input[i]>=0){
					if (i==input.length-1){
							arrTemp[sum] = each;
							sum++;
					}	
				} else {
					break;
				}
			}
		}
		int[] arrRet = new int[sum];
		System.arraycopy(arrTemp, 0, arrRet, 0, sum);
		return arrRet;
	}

	public static int[] getNumsDivisionWithoutX(int[] input, int x) {
		// Without collections
		int[] arrTemp = new int[input.length];

		int sum=0;
		for (int each:input){
			for (int i=0;i<input.length;i++){
				if (each/input[i]!=x){
					if (i==input.length-1){
							arrTemp[sum] = each;
							sum++;
					}	
				} else {
					break;
				}
			}
		}

		int[] arrRet = new int[sum];
		System.arraycopy(arrTemp, 0, arrRet, 0, sum);
		return arrRet;
	}

	public static String[] getStringWithXlength(String input, int x) {
		String[] arrInput = input.split(" ");
		String[] arrTemp = new String[arrInput.length];

		int sum=0;
		for (String each : arrInput) {
			if (each.length()==x) {
				arrTemp[sum] = each;
				sum++;
			}
		}

		String[] arrRet = new String[sum];
		System.arraycopy(arrTemp, 0, arrRet, 0, sum);
		return arrRet;
	}
}